import moment from "moment";

export function getDateObj(date, time, inc = false) {
  console.log("datedate", date);
  console.log("time", time);

  if (inc)
    return moment(`${date}, ${time}`, "YYYY/MM/DD, HH:mm").add(30, "minutes");
  else return moment(`${date}, ${time}`, "YYYY/MM/DD, HH:mm");
}

export function getFormattedDate(date, format) {
  console.log("datedate", date);
  console.log("datformatformatedate", format);
  return moment(date).format(format ? format : "YYYY-MM-DD");
}

export function getTimeDiff(endTime, startTime) {
  var startTime = moment(startTime, "HH:mm:ss a");
  var endTime = moment(endTime, "HH:mm:ss a");
  var duration = moment.duration(endTime.diff(startTime));
  var hours = parseInt(duration.asHours());
  var minutes = parseInt(duration.asMinutes()) - hours * 60;
  return hours + "h " + minutes + "m";
}
