import React, { useState, useEffect, useReducer } from "react";
import Constants from "./../Constants";

const useFetch = () => {
  const { URL } = Constants;
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const res = await fetch(URL);
        const json = await res.json();
        setResponse(json);
        console.log({ json });
        setIsLoading(false);
      } catch (error) {
        setError(error);
      }
    };
    fetchData();
  }, []);
  return { response, error, isLoading };
};
export default useFetch;
