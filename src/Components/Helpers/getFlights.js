import { getDateObj } from "./dateHelpers";
import moment from "moment";
function getFlights(state, res) {
  let arr = res.response;
  console.log("res=======>", arr);
  if (arr && arr.length) {
    const FullFlight = arr.filter(
      (val) =>
        val.destination === state.destination &&
        val.origin === state.origin &&
        val.date === state.departureDate
    );
    console.log("FullFlight=======>", FullFlight);

    const destinationMatch = arr.filter(
      (val) =>
        val.destination === state.destination &&
        val.date === state.departureDate
    );
    console.log("destinationMatch=======>", destinationMatch);

    const originMatch = arr.filter(
      (val) => val.origin === state.origin && val.date === state.departureDate
    );
    console.log("originMatch=======>", originMatch);

    let multipleValidCkpt = [];

    originMatch.map((originVal) => {
      destinationMatch.map((destinationVal) => {
        if (
          originVal.origin === state.origin &&
          originVal.destination === destinationVal.origin &&
          destinationVal.destination === state.destination
        ) {
          let originValDateString = getDateObj(
            originVal.date,
            originVal.arrivalTime,
            true
          );
          let destinationValDateString = getDateObj(
            destinationVal.date,
            destinationVal.departureTime
          );
          console.log(
            "originValDateStringoriginValDateString",
            originValDateString
          );
          console.log(
            "destinationValDateStringdestinationValDateStringdestinationValDateString",
            destinationValDateString
          );
          if (originValDateString.isSameOrBefore(destinationValDateString)) {
            const FlightDetails = {
              origin: originVal.origin,
              name: "Multiple",
              type: "Connecting",
              departureTime: originVal.departureTime,
              arrivalTime: destinationVal.arrivalTime,
              destination: destinationVal.destination,
              price: originVal.price + destinationVal.price,
            };
            multipleValidCkpt = [
              ...FullFlight,
              ...multipleValidCkpt,
              {
                ...FlightDetails,
                route1: originVal,
                route2: destinationVal,
              },
            ];
          }
        }
      });
    });
    return multipleValidCkpt;
    console.log("multipleValidCkpt=======>", multipleValidCkpt);
  }
}
export default getFlights;
