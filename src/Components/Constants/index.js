const Constants = {
  URL: "https://tw-frontenders.firebaseio.com/advFlightSearch.json",
  DATE_FORMAT: "YYYY/MM/DD",
  TIME_FORMAT: "HH:mm",
  STATIONS: ["Pune (PNQ)", "Mumbai (BOM)", "Bengaluru (BLR)", "Delhi (DEL)"],
};
export default Constants;
