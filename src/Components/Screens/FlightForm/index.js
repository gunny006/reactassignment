import React, { useState, useEffect, useReducer } from "react";
import FlightDetail from "../../Common/CardDetails";
import TextField from "@material-ui/core/TextField";
import moment from "moment";
import DropDown from "../../Common/DropDown";
import getFlights from "./../../Helpers/getFlights";
import {
  PageHeader,
  PageHeaderText,
  DualLayout,
  FilterWrapper,
  DetailWrapper,
  FilterBoxTabs,
  FilterBox,
  FilterForm,
  FilterInput,
  FilterButton,
  FilterPicker,
  TabPills,
  FlightJourneyHeader,
  JourneyText,
  JourneyTextSub,
  FlightDetailBody,
} from "./styles";
import useFetch from "./../../Helpers/getFlightData";
import Constants from "./../../Constants";
import { reducer } from "./formReducer";
import { getDateObj } from "./../../Helpers/dateHelpers";
import DatePicker from "./../../Common/DatePicker";
const FlightSearch = () => {
  const { STATIONS } = Constants;
  const res = useFetch();
  console.log("resresresresresresres", res);

  console.log("STATIONS", STATIONS);

  const initialState = {
    flightType: 1,
    origin: "Bengaluru (BLR)",
    destination: "Delhi (DEL)",
    departureDate: "2020/11/02",
    returnDate: "",
    passengers: "10",
    validFlights: [],
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    // let data = getFlights(state, res);
    // // dispatch({ type: "VALIDFLIGHTS", data: type });
    // dispatch({ type: "VALIDFLIGHTS", data: data });
    // console.log("datadatadatadatadatadatadata", data);
  }, [res]);
  console.log({ state });
  return (
    <div style={{ backgroundColor: "#fff", height: "100vh" }}>
      <PageHeader>
        <PageHeaderText>Flight Search App</PageHeaderText>
      </PageHeader>
      <DualLayout>
        <FilterWrapper>
          <FilterBox>
            <FilterBoxTabs>
              <TabPills
                style={{
                  background: state.flightType == 1 ? "#2196F3" : "none",
                  borderColor: state.flightType == 1 ? "#2196F3" : "#000",
                }}
                onClick={() => {
                  dispatch({ type: "FLIGHTTYPE", data: 1 });
                }}
              >
                <span>One Way</span>
              </TabPills>
              <TabPills
                style={{
                  background: state.flightType == 2 ? "#2196F3" : "none",
                  borderColor: state.flightType == 2 ? "#2196F3" : "#000",
                }}
                onClick={() => {
                  dispatch({ type: "FLIGHTTYPE", data: 2 });
                }}
              >
                <span>Return</span>
              </TabPills>
            </FilterBoxTabs>

            <FilterForm>
              <DropDown
                label={"Origin"}
                onSelect={(val) => {
                  dispatch({ type: "ORIGIN", data: val });
                }}
              />
              <DropDown
                label={"Destination"}
                onSelect={(val) => dispatch({ type: "DESTINATION", data: val })}
              />
              <DatePicker
                label="Departure Date"
                initialDate={new Date()}
                onSelect={(value) => {
                  dispatch({ type: "DEPDATE", data: value });
                }}
              />

              {state.flightType == 2 && (
                <DatePicker
                  label="Return Date"
                  onSelect={(value) => {
                    dispatch({ type: "RETURNDATE", data: value });
                  }}
                />
              )}

              <DropDown
                label={"Passengers"}
                data={["1", "2", "3", "4", "5"]}
                onSelect={(val) => {
                  dispatch({ type: "PASSENGERS", data: val });
                }}
              />
              <FilterButton>
                <button
                  onClick={() => {
                    let data = getFlights(state, res);

                    dispatch({ type: "VALIDFLIGHTS", data: data });
                  }}
                  title="Search"
                >Search</button>
              </FilterButton>
            </FilterForm>
          </FilterBox>
        </FilterWrapper>
        <DetailWrapper>
          <FlightJourneyHeader>
            <div>
              <JourneyText>
                {`${state.origin} to ${state.destination}`}
              </JourneyText>
              <div
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <JourneyTextSub>
                  {state.validFlights.length} flights found
                </JourneyTextSub>
                <JourneyTextSub>{state.departureDate}</JourneyTextSub>
              </div>
            </div>
          </FlightJourneyHeader>
          <FlightDetailBody>
            {state.validFlights.map((data, index) => (
              <FlightDetail flight={data} />
            ))}
          </FlightDetailBody>
        </DetailWrapper>
      </DualLayout>
    </div>
  );
};

export default FlightSearch;
