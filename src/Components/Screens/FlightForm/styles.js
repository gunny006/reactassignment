import React from "react";
import styled from "styled-components";
import getPlatformType from "../../Helpers/getPlatform";

const layoutType = getPlatformType();
let PageHeader,
  PageHeaderText,
  DualLayout,
  FilterWrapper,
  DetailWrapper,
  FilterBox,
  FilterBoxTabs,
  FilterInput,
  FilterButton,
  FilterPicker,
  TabPills,
  FlightJourneyHeader,
  JourneyText,
  JourneyTextSub,
  FlightDetailBody,
  FilterForm;

PageHeader = styled.div`
  padding: 10px 5px;
`;
PageHeaderText = styled.div`
  font-size: 18px;
  color: #000;
  font-weight: 500;
`;
DualLayout = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  border: 1px solid black;
`;
FilterWrapper = styled.div`
  width: 20%;
  border-right: 1px solid black;
`;
DetailWrapper = styled.div`
  width: 80%;
`;
FilterBox = styled.div`
  padding: 20px 3px;
  border-color: black;
  border-right-width: 1px;
`;
FilterBoxTabs = styled.div`
 display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;
FilterForm = styled.div`
  padding: 5px;
  border: 1px solid #000;
`;
FilterInput = styled.input`
  border: 1px solid black;
  padding: 3px;
`;
FilterPicker = styled.select`
  padding: 3px;
`;
FilterButton = styled.div`
  width: 80px;
  border-radius: 4px;
  margin: 15px 0px;
  overflow: hidden;
`;
TabPills = styled.div`
  padding: 7px 12px;
  border: 1px solid #000;
  border-bottom-width: 0px;
  border-top-left-radius-: 2px;
  border-top-right-radius-: 2px;
`;
FlightJourneyHeader = styled.div`
display: flex;
  align-items: flex-start;
  justify-content: center;
  padding: 10px;
  border-color: #000;
  border-bottom-width: 1px;
`;
JourneyText = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: #000;
`;
JourneyTextSub = styled.div`
  font-size: 10px;
  color: grey;
`;
FlightDetailBody = styled.div`
  padding: 5px 0px;
`;

if (layoutType == "phone") {
  FilterWrapper = styled.div`
    width: 100%;
    border-right: 1px solid black;
  `;
  DetailWrapper = styled.div`
    width: 100%;
  `;
  DualLayout = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    border: 1px solid black;
  `;
}

export {
  PageHeader,
  PageHeaderText,
  DualLayout,
  FilterWrapper,
  DetailWrapper,
  FilterBoxTabs,
  FilterBox,
  FilterInput,
  FilterForm,
  FilterButton,
  FilterPicker,
  TabPills,
  FlightJourneyHeader,
  JourneyText,
  JourneyTextSub,
  FlightDetailBody,
};
