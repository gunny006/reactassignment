export function reducer(state, action) {
  switch (action.type) {
    case "ORIGIN":
      return {
        ...state,
        origin: action.data,
      };
    case "DESTINATION":
      return {
        ...state,
        destination: action.data,
      };
    case "DEPDATE":
      return {
        ...state,
        departureDate: action.data,
      };
    case "RETURNDATE":
      return {
        ...state,
        returnDate: action.data,
      };
    case "PASSENGERS":
      return {
        ...state,
        passengers: action.data,
      };
    case "FLIGHTTYPE":
      return {
        ...state,
        flightType: action.data,
      };
    case "VALIDFLIGHTS":
      return {
        ...state,
        validFlights: action.data,
      };

    default:
      return state;
  }
}
