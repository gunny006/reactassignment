import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Constants from "./../../Constants";

const options = Constants.STATIONS;

export default function DropDown(props) {
  const [value, setValue] = React.useState();
  const [inputValue, setInputValue] = React.useState("");
  const { label, onSelect, data } = props;
  return (
    <Autocomplete
      freeSolo
      options={data ? data : options}
      value={value}
      style={{ width: "100%", marginTop: 10 }}
      onChange={(event, newValue) => {
        setValue(newValue);
        onSelect(newValue);
      }}
      inputValue={inputValue}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      id="free-solo-2-demo"
      disableClearable
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          margin="normal"
          variant="outlined"
          // InputProps={{ ...params.InputProps, type: "search" }}
        />
      )}
    />
  );
}
