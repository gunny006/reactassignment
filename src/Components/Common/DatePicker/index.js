import React from "react";
import TextField from "@material-ui/core/TextField";
import { getFormattedDate } from "./../../Helpers/dateHelpers";
export default function DatePicker(props) {
  const [value, setValue] = React.useState();
  const [inputValue, setInputValue] = React.useState("");
  const { label, onSelect, initialDate } = props;
  return (
    <TextField
      id="date"
      label={label}
      type="date"
      onChange={(e) => {
        const val = e.target.value;
        onSelect(getFormattedDate(val, "YYYY/MM/DD"));
      }}
      defaultValue={getFormattedDate(initialDate)}
      style={{ width: "100%", marginTop: 20 }}
      InputLabelProps={{
        shrink: true,
      }}
    />
  );
}
