import React from "react";
import styled from "styled-components";
import getPlatformType from "../../Helpers/getPlatform";
const layoutType = getPlatformType();

let MainBox,
  ImgBox,
  FlightNameBox,
  FlightName,
  FlightCode,
  FlightDepartureBox,
  FlightArrivalBox,
  DepartureTime,
  DepartureCity,
  ArrivalTime,
  ArrivalCity,
  JourneyTypeBox,
  JourneyHours,
  JourneyType,
  FlightRateBox,
  FlightRate,
  FlightBookBtnBox,
  LayoverBox,
  LayoverDuration,
  LayoverRow;

MainBox = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  margin: 5px 10px;
  border: 1px solid black;
`;
ImgBox = styled.div`
  padding: 5px 10px;
  width: 14%;
`;
FlightNameBox = styled.div`
  width: 14%;
  padding: 5px 10px;
`;

FlightName = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: #000;
`;

FlightCode = styled.div`
  font-size: 10px;
  color: grey;
`;

FlightDepartureBox = styled.div`
  width: 14%;
  padding: 5px 10px;
`;

DepartureTime = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: #000;
`;
DepartureCity = styled.div`
  font-size: 10px;
  color: grey;
`;

FlightArrivalBox = styled.div`
  width: 14%;
  padding: 5px 10px;
`;
ArrivalTime = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: #000;
`;
ArrivalCity = styled.div`
  font-size: 10px;
  color: grey;
`;
JourneyTypeBox = styled.div`
  width: 14%;
  padding: 5px 10px;
`;

JourneyHours = styled.div``;

JourneyType = styled.div``;

FlightRateBox = styled.div`
  width: 14%;
  padding: 5px 10px;
`;

FlightRate = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: #000;
`;

FlightBookBtnBox = styled.div`
  width: 14%;
  padding: 5px 10px;
`;
LayoverBox = styled.div`
  width: 100%;
`;

LayoverRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  background: #f1f1f1;
`;
LayoverDuration = styled.div`
  position: absolute;
  left: 40%;
  bottom: -11px;
  background: #f9f9f9;
  padding: 10px;
  border: 1px dashed #c3c3c3;
  padding: 4px 10px;
  font-size: 10px;
  z-index: 99;
`;

if (layoutType == "phone") {
  MainBox = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    flex-wrap: wrap;
    margin: 5px 10px;
    border: 1px solid black;
  `;
  ImgBox = styled.div`
    padding: 5px 10px;
    width: 100%;
  `;
  FlightNameBox = styled.div`
    width: 100%;
    padding: 5px 10px;
  `;

  FlightName = styled.div`
    font-size: 14px;
    font-weight: 500;
    color: #000;
  `;

  FlightCode = styled.div`
    font-size: 10px;
    color: grey;
  `;

  FlightDepartureBox = styled.div`
    width: 100%;
    padding: 5px 10px;
  `;

  DepartureTime = styled.div`
    font-size: 14px;
    font-weight: 500;
    color: #000;
  `;
  DepartureCity = styled.div`
    font-size: 10px;
    color: grey;
  `;

  FlightArrivalBox = styled.div`
    width: 100%;
    padding: 5px 10px;
  `;
  ArrivalTime = styled.div`
    font-size: 14px;
    font-weight: 500;
    color: #000;
  `;
  ArrivalCity = styled.div`
    font-size: 10px;
    color: grey;
  `;
  JourneyTypeBox = styled.div`
    width: 100%;
    padding: 5px 10px;
  `;

  JourneyHours = styled.div``;

  JourneyType = styled.div``;

  FlightRateBox = styled.div`
    width: 100%;
    padding: 5px 10px;
  `;

  FlightRate = styled.div`
    font-size: 14px;
    font-weight: 500;
    color: #000;
  `;

  FlightBookBtnBox = styled.div`
    width: 100%;
    padding: 5px 10px;
  `;
  LayoverBox = styled.div`
    width: 100%;
  `;

  LayoverRow = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    flex-wrap: wrap;
    background: #f1f1f1;
  `;
  LayoverDuration = styled.div`
    position: absolute;
    left: 40%;
    bottom: -11px;
    background: #f9f9f9;
    padding: 10px;
    border: 1px dashed #c3c3c3;
    padding: 4px 10px;
    font-size: 10px;
    z-index: 99;
  `;
}

export {
  MainBox,
  ImgBox,
  FlightNameBox,
  FlightName,
  FlightCode,
  FlightDepartureBox,
  DepartureTime,
  DepartureCity,
  ArrivalTime,
  ArrivalCity,
  JourneyTypeBox,
  JourneyHours,
  JourneyType,
  FlightRateBox,
  FlightRate,
  FlightBookBtnBox,
  FlightArrivalBox,
  LayoverBox,
  LayoverRow,
  LayoverDuration,
};
