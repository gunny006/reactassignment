import React, { useState, useEffect, useReducer } from "react";
import { getTimeDiff } from "./../../Helpers/dateHelpers";
import {
  MainBox,
  ImgBox,
  FlightNameBox,
  FlightName,
  FlightCode,
  FlightDepartureBox,
  DepartureTime,
  DepartureCity,
  FlightArrivalBox,
  ArrivalTime,
  ArrivalCity,
  JourneyTypeBox,
  JourneyHours,
  JourneyType,
  FlightRateBox,
  FlightRate,
  FlightBookBtnBox,
  LayoverBox,
  LayoverDuration,
  LayoverRow,
} from "./styles";

const FlightDetail = (data) => {
  console.log("asdasdasdasda ", data);
  const {
    destination,
    departureTime,
    arrivalTime,
    name,
    origin,
    price,
    route1,
    route2,
    flightNo,
    type,
  } = data.flight;
  const [showLayoverCard, setLayoverCard] = useState(false);
  return (
    <>
      <MainBox>
        <ImgBox>
          <img
            source={require(type
              ? "./../../Assests/multiple.png"
              : "./../../Assests/airplane.png")}
            resizeMode="contain"
            style={{ width: 60, height: 60 }}
          />
        </ImgBox>
        <FlightNameBox>
          <FlightName>{name}</FlightName>
          <FlightCode>{flightNo}</FlightCode>
          {type && (
            <FlightCode onClick={() => setLayoverCard(!showLayoverCard)}>
              {setLayoverCard ? "Show Details" : "Hide Details"}
            </FlightCode>
          )}
        </FlightNameBox>
        <FlightDepartureBox>
          <DepartureTime>{departureTime}</DepartureTime>
          <DepartureCity>{origin}</DepartureCity>
        </FlightDepartureBox>
        <FlightArrivalBox>
          <ArrivalTime>{arrivalTime}</ArrivalTime>
          <ArrivalCity>{destination}</ArrivalCity>
        </FlightArrivalBox>
        <JourneyTypeBox>
          <JourneyHours>{getTimeDiff(arrivalTime, departureTime)}</JourneyHours>
          <JourneyType>{type ? type : "Non stop"}</JourneyType>
        </JourneyTypeBox>
        <FlightRateBox>
          <FlightRate>${price}</FlightRate>
        </FlightRateBox>
        <FlightBookBtnBox>
          {/* <Button
            style={{ background: "red" }}
            onPress={console.log("pressed")}
            title="Book"
          /> */}
        </FlightBookBtnBox>
        {type && showLayoverCard && (
          <LayoverBox>
            <LayoverRow
              style={{
                borderBottomStyle: "dashed",
                borderBottomWidth: 1,
                borderBottomColor: "#c3c3c3",
                borderTopStyle: "solid",
                borderTopWidth: 1,
                borderTopColor: "#000",
              }}
            >
              <ImgBox>
                <img
                  source={require("./../../Assests/airplane.png")}
                  resizeMode="contain"
                  style={{ width: 60, height: 60 }}
                />
              </ImgBox>
              <FlightNameBox>
                <FlightName>{route1.name}</FlightName>
                <FlightCode>{route1.flightNo}</FlightCode>
              </FlightNameBox>
              <FlightDepartureBox>
                <DepartureTime>{route1.departureTime}</DepartureTime>
                <DepartureCity>{route1.origin}</DepartureCity>
              </FlightDepartureBox>
              <FlightArrivalBox>
                <ArrivalTime>{route1.arrivalTime}</ArrivalTime>
                <ArrivalCity>{route1.destination}</ArrivalCity>
              </FlightArrivalBox>
              <JourneyTypeBox>
                <JourneyHours>
                  {getTimeDiff(route1.arrivalTime, route1.departureTime)}
                </JourneyHours>
                <JourneyType>Non stop</JourneyType>
              </JourneyTypeBox>
              <LayoverDuration>
                Layover time{" "}
                {getTimeDiff(route2.departureTime, route1.arrivalTime)}
              </LayoverDuration>
            </LayoverRow>
            <LayoverRow style={{ zIndex: -1 }}>
              <ImgBox>
                <img
                  source={require("./../../Assests/airplane.png")}
                  resizeMode="contain"
                  style={{ width: 60, height: 60 }}
                />
              </ImgBox>
              <FlightNameBox>
                <FlightName>{route2.name}</FlightName>
                <FlightCode>{route2.flightNo}</FlightCode>
              </FlightNameBox>
              <FlightDepartureBox>
                <DepartureTime>{route2.departureTime}</DepartureTime>
                <DepartureCity>{route2.origin}</DepartureCity>
              </FlightDepartureBox>
              <FlightArrivalBox>
                <ArrivalTime>{route2.arrivalTime}</ArrivalTime>
                <ArrivalCity>{route2.destination}</ArrivalCity>
              </FlightArrivalBox>
              <JourneyTypeBox>
                <JourneyHours>02h 00m</JourneyHours>
                <JourneyType>Non stop</JourneyType>
              </JourneyTypeBox>
            </LayoverRow>
          </LayoverBox>
        )}
      </MainBox>
    </>
  );
};

export default FlightDetail;
