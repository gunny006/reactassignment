import React from "react";
import logo from "./logo.svg";
import FlightSearch from "./Components/Screens/FlightForm";

import "./App.css";

function App() {
  return (
    <>
      <FlightSearch />
    </>
  );
}

export default App;
